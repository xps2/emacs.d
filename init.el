;;; init.el --- Emacs configuration
;;; Commentary:
;;; Code:

;; proxy
;;(setq url-proxy-services
;;      '(("http" . "127.0.0.1:50000")
;;        ("https" . "127.0.0.1:50000")))
;;(setq url-http-proxy-basic-auth-storage
;;      '(("127.0.0.1:50000" ("Proxy" . "base64string"))))

;; package.el
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; use-package
(unless package-archive-contents (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; package-selected-packagesをinit.elからcustom.elに分離
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package nginx-mode
  :ensure t)

(use-package typescript-mode
  :ensure t)

(use-package editorconfig
  :ensure t
  :config (editorconfig-mode t))

;; emacs-mozcが必要
(use-package mozc
  :disabled t
  :if (not (eq system-type 'windows-nt))
  :config (setq default-input-method "japanese-mozc"))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; Standard Jedi.el setting
;; Type:
;;     M-x jedi:install-server RET
;; Then open Python file.
(use-package jedi
  :ensure t
  :config
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:complete-on-dot t))

(use-package json-mode
  :ensure t)

(use-package yaml-mode
  :ensure t
  :mode ("\\.yml\\'" . yaml-mode)
  :config
  (add-hook 'yaml-mode-hook
            '(lambda ()
               (define-key yaml-mode-map "\C-m" 'newline-and-indent))))

(use-package vue-mode
  :ensure t
  :config
  ;; 0, 1, or 2, representing (respectively) none, low, and high coloring
  (setq mmm-submode-decoration-level 0))

(use-package stylus-mode
  :ensure t)

(use-package tornado-template-mode
  :ensure t
  :disabled t
  :mode ("\\.html\\'" . tornado-template-mode))

(use-package matlab-mode
  :ensure t
  :disabled t
  :diminish matlab
  :mode ("\\.m\\'" . matlab-mode)
  :commands (matlab-shell)
  :config
  (autoload 'matlab-mode "matlab" "Enter MATLAB mode." t)
  (autoload 'matlab-shell "matlab" "Interactive MATLAB mode." t))

(use-package elscreen
  :ensure t
  :disabled t
  :if window-system
  :config
  (setq elscreen-prefix-key (kbd "C-z"))
  (define-key elscreen-map (kbd "C-z") 'iconify-or-deiconify-frame)
  (define-key elscreen-map (kbd "C-z") 'suspend-emacs))

(use-package rainbow-delimiters
  :ensure t
  :config (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package nlinum
  :ensure t
  :config (global-nlinum-mode t))

(use-package whitespace
  :config
  (setq whitespace-style
        '(face ; faceで可視化
          trailing ; 行末
          tabs ; タブ
          spaces ; スペース
          empty
          space-mark ; 表示のマッピング
          tab-mark))
  (setq whitespace-display-mappings
        '((space-mark ?\u3000 [?\u2423])
          (tab-mark ?\t [?\u00BB ?\t] [?\\ ?\t]))
        )
  (setq whitespace-space-regexp "\\(\u3000+\\)")
  (setq whitespace-action '(auto-cleanup))
  (set-face-attribute 'whitespace-tab nil
                      :background "SteelBlue"
                      :underline t)
  (set-face-attribute 'whitespace-space nil
                      :foreground "LightBlue"
                      :background "LightBlue"
                      :underline nil)
  (global-whitespace-mode t)
  )

;;; 対応する括弧の強調
(use-package paren
  :config
  (show-paren-mode 1))

(use-package pdf-preview
  :disabled t)

;;; menu-tree.el
;; メニュー日本語化
;; http://www11.atwiki.jp/s-irie/pages/13.html
(use-package menu-tree
  :disabled t)

;;keisen.el Shift+矢印で簡単に罫線を描画
(use-package keisen
  :disabled t
  :config
  (global-set-key [S-right] 'keisen-right-move )
  (global-set-key [S-left] 'keisen-left-move )
  (global-set-key [S-up] 'keisen-up-move )
  (global-set-key [S-down] 'keisen-down-move )
  (autoload 'keisen-up-move "keisen" nil t)
  (autoload 'keisen-down-move "keisen" nil t)
  (autoload 'keisen-left-move "keisen" nil t)
  (autoload 'keisen-right-move "keisen" nil t)
)

;;; hide menu bar
;;(menu-bar-mode nil)
 ;;; hide tool bar
;;(tool-bar-mode nil)

;;; modified version
;;; for details, see: http://ntemacsjp.sourceforge.jp/matsuan/FontSettingJp.html
(if nil
    (progn
      (create-fontset-from-ascii-font
       "-outline-Consolas-normal-normal-normal-mono-13-*-*-*-c-*-iso8859-1"
       ;;"-outline-Consolas-normal-normal-normal-mono-12-*-*-*-*-*-iso8859-1"
       nil "myfont")
      (set-fontset-font "fontset-myfont"
                        'japanese-jisx0208
                        '("ＭＳ ゴシック*" . "unicode-bmp")
                        )

      (set-fontset-font "fontset-myfont"
                        'katakana-jisx0201
                        '("ＭＳ ゴシック*" . "unicode-bmp")
                        )
      (add-to-list 'default-frame-alist '(font . "fontset-myfont"))
      )
  )

;;(set-face-attribute'default nil
;;                    :family "Source Han Code JP R"
;;                    )

(setq default-frame-alist
      (append (list '(width . 80)
                    '(height . 49)
                    '(alpha . 85)
                    '(foreground-color . "white")
                    '(background-color . "black")
                    '(background-mode . "dark")
                    '(cursor-color . "white")
                    )
              default-frame-alist))

;;; disable GNU splash screen
;; for details, see
;; http://www.gnu.org/s/emacs/manual/html_node/elisp/Startup-Summary.html
;;(setq inhibit-startup-screen t)
(setq initial-buffer-choice nil)

;;; for file save for zenkaku tilde (cp932=MS-Windows-SJIS)
;;(prefer-coding-system 'cp932-dos)

(set-language-environment "Japanese")
(prefer-coding-system 'utf-8-unix)
(setq-default buffer-file-coding-system 'utf-8-unix)

;; UTF-8⇔Legacy Encoding (EUC-JP や Shift_JIS など)をWindowsで変換
;;http://nijino.homelinux.net/emacs/emacs23-ja.html
;; encode-translation-table の設定
;;(coding-system-put 'euc-jp :encode-translation-table
;;                   (get 'japanese-ucs-cp932-to-jis-map 'translation-table))
;;(coding-system-put 'iso-2022-jp :encode-translation-table
;;                   (get 'japanese-ucs-cp932-to-jis-map 'translation-table))
;;(coding-system-put 'cp932 :encode-translation-table
;;                   (get 'japanese-ucs-jis-to-cp932-map 'translation-table))
;; charset と coding-system の優先度設定
;;(set-charset-priority 'ascii 'japanese-jisx0208 'latin-jisx0201
;;                      'katakana-jisx0201 'iso-8859-1 'cp1252 'unicode)
;;(set-coding-system-priority 'utf-8 'euc-jp 'iso-2022-jp 'cp932)

;;; 検索において，大文字・小文字の区別しない．
(setq-default case-fold-search t)

;;; yes-or-noをy-or-nに
(fset 'yes-or-no-p 'y-or-n-p)

;; ---------------------------------------------
;; 印刷設定
;;  Ghostscriptが必要
;;   http://auemath.aichi-edu.ac.jp/~khotta/ghost/
;;  BoldFontが必要
;;   http://ftp.yz.yamagata-u.ac.jp/pub/GNU/intlfonts/intlfonts-1.2.1.tar.gz
;;
;;     M-x ps-print-buffer           バッファを白黒印刷
;;     M-x ps-print-buffer-with-face バッファをカラー印刷
;;     M-x ps-print-region           リージョンを白黒印刷
;;     M-x ps-print-region-with-face リージョンをカラー印刷
;; ---------------------------------------------

;; ghostscriptの実行コマンド場所を指定
(setq ps-print-color-p t
      ps-lpr-command "C:/msys64/mingw64/bin/gswin32c.exe"
      ps-multibyte-buffer 'non-latin-printer
      ps-lpr-switches '("-sDEVICE=mswinpr2" "-dNOPAUSE" "-dBATCH" "-dWINKANJI")
      printer-name nil
      ps-printer-name nil
      ps-printer-name-option nil
      ps-print-header nil          ; ヘッダの非表示
      )

;;IMEの設定
;; http://snak.tdiary.net/20100716.html
;; http://d.hatena.ne.jp/end0tknr/20100815/1281872885
;; http://www-tsujii.is.s.u-tokyo.ac.jp/~yoshinag/tips/dot_emacs.html
(when (fboundp 'w32-ime-initialize)
  (setq default-input-method "W32-IME")
  (setq-default w32-ime-mode-line-state-indicator "[--]")
  (setq w32-ime-mode-line-state-indicator-list '("[--]" "[あ]" "[--]"))
  (w32-ime-initialize)
  (setq w32-ime-buffer-switch-p nil)    ;バッファ切り替え時にIME状態を引き継ぐ
  (add-hook 'input-method-activate-hook
            (lambda() (set-cursor-color "yellow")))
  (add-hook 'input-method-inactivate-hook
            (lambda() (set-cursor-color "white")))
  (global-set-key (kbd "C-o") 'toggle-input-method)
  ;; IMEの制御（yes/noをタイプするところでは IME をオフにする）
  (wrap-function-to-control-ime 'universal-argument t nil)
  (wrap-function-to-control-ime 'read-string nil nil)
  (wrap-function-to-control-ime 'read-char nil nil)
  (wrap-function-to-control-ime 'read-from-minibuffer nil nil)
  (wrap-function-to-control-ime 'y-or-n-p nil nil)
  (wrap-function-to-control-ime 'yes-or-no-p nil nil)
  (wrap-function-to-control-ime 'map-y-or-n-p nil nil)
  )

;; 改行時オートインデント設定
(setq-default indent-tabs-mode nil)
(setq indent-line-function 'indent-relative-maybe)
(global-set-key (kbd "C-m") 'newline-and-indent); Returnキーで改行＋オートインデント
(global-set-key (kbd "C-m") 'indent-new-comment-line); Returnキーで改行＋オートインデント＋コメント行

(global-set-key (kbd "C-z") 'undo)                           ;;UNDO
(global-set-key (kbd "C-h") 'backward-delete-char)           ;;Ctrl-Hでバックスペース
(global-set-key (kbd "C-x C-l") 'goto-line)                  ;;指定行へ移動

;; 初期ディレクトリを設定
;; ショートカットで作業ディレクトリを指定しても問答無用で
;; 以下に指定したディレクトリになるので注意
(setq default-directory "~/")

;;; ファイルのフルパスをタイトルバーに表示
;;http://d.hatena.ne.jp/sandai/20101015/p1
;;http://www-tsujii.is.s.u-tokyo.ac.jp/~yoshinag/tips/dot_emacs.html
(setq frame-title-format
      `(" %b " (buffer-file-name "[%f]") "  ---  emacs-"emacs-version"@",(system-name)))

(line-number-mode t)
(column-number-mode t)

;;(load "emacs-wiki.el")
;;(add-hook 'emacs-wiki-mode-hook 'outline-minor-mode)
;;(add-hook 'emacs-wiki-mode-hook 'highlight-changes-mode)

;;(load-library "pukiwiki-mode")

(provide 'init)
;;; init.el ends here
